﻿namespace MeetingRoomSolver;

public record MeetingTime(
    int StartTime, 
    int EndTime);